// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "p2GameMode.generated.h"

UCLASS(minimalapi)
class Ap2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Ap2GameMode();
};



