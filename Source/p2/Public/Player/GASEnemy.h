// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/GASCharacterBase.h"
#include "GASEnemy.generated.h"

/**
 * 
 */
UCLASS()
class P2_API AGASEnemy : public AGASCharacterBase
{
	GENERATED_BODY()

public:
	AGASEnemy();

	virtual void PossessedBy(AController* NewController) override;

protected:
	virtual void BeginPlay() override;
	virtual void InitAbilityActorInfo() override;
};
