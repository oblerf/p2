// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Player/GASCharacterBase.h"
#include "GASCharacter.generated.h"

/**
 * 
 */
UCLASS()
class P2_API AGASCharacter : public AGASCharacterBase
{
	GENERATED_BODY()
public:
	AGASCharacter();
	virtual void PossessedBy(AController* NewController) override;
	virtual void OnRep_PlayerState() override;

	virtual void InitAbilityActorInfo() override;

};
