// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GASPlayerController.generated.h"

class UGASAbilitySystemComponent;
/**
 * 
 */
UCLASS()
class P2_API AGASPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	AGASPlayerController();

protected:
	UFUNCTION(BlueprintCallable)
	void AbilityInputTagPressed(FGameplayTag InputTag);
	UFUNCTION(BlueprintCallable)
	void AbilityInputTagReleased(FGameplayTag InputTag);

	UGASAbilitySystemComponent* GetGASASC();

private:
	UPROPERTY()
	TObjectPtr<UGASAbilitySystemComponent> GASAbilitySystemComponent;

};
