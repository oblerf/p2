#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"

struct FGASGameplayTags
{
public:
	static const FGASGameplayTags& Get() { return GameplayTags; }
	static void InitializeNativeGameplayTags();

	FGameplayTag Attributes_Secondary_Attack;
	FGameplayTag Attributes_Secondary_Defense;
	FGameplayTag Attributes_Secondary_MaxHealth;

	FGameplayTag Ability_Attack;

	FGameplayTag Damage_BaseDamage;

	FGameplayTag Equip_Weapon_1;

private:
	static FGASGameplayTags GameplayTags;
};
