// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GASAbilityTypes.h"
#include "GameplayAbility/GASGameplayAbility.h"
#include "GASDamageGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class P2_API UGASDamageGameplayAbility : public UGASGameplayAbility
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void CauseDamage(AActor* TargetActor);
	UFUNCTION(BlueprintCallable)
	bool GetTraceData(FVector& TraceStart, FVector& TraceEnd) const;
	UFUNCTION(BlueprintCallable)
	bool GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const;
	UFUNCTION(BlueprintCallable)
	void MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd) const;

	UFUNCTION(BlueprintPure)
	FDamageEffectParams MakeDamageEffectParamsFromClassDefaults(AActor* TargetActor = nullptr) const;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Damage")
	TSubclassOf<UGameplayEffect> DamageEffectClass;
	UPROPERTY(EditDefaultsOnly, Category="Damage")
	FGameplayTag DamageTag;
	UPROPERTY(EditDefaultsOnly, Category="Damage")
	FScalableFloat DamageValue;
	UPROPERTY(EditDefaultsOnly, Category="Damage")
	float DeathImpulseMagnitude = 5000.f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Damage")
	float TraceMaxDistance = 1000.f;
};
