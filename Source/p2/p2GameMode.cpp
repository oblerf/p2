// Copyright Epic Games, Inc. All Rights Reserved.

#include "p2GameMode.h"
#include "p2Character.h"
#include "UObject/ConstructorHelpers.h"

Ap2GameMode::Ap2GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
