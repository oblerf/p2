// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class p2 : ModuleRules
{
	public p2(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[]
			{ "Core", "CoreUObject", "Engine", "InputCore", "EnhancedInput", "GameplayAbilities" });
		PrivateDependencyModuleNames.AddRange(new string[]
			{ "GameplayTags", "GameplayTasks", "NavigationSystem", "Niagara","AIModule" });

	}
}