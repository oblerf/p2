// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/GASCharacter.h"

#include "AbilitySystemComponent.h"
#include "GASAbilitySystemComponent.h"
#include "Player/GASPlayerState.h"

AGASCharacter::AGASCharacter()
{
}

void AGASCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	InitAbilityActorInfo();
	AddCharacterAbilities();

}

void AGASCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	InitAbilityActorInfo();

}

void AGASCharacter::InitAbilityActorInfo()
{
	AGASPlayerState* GASPlayerState = GetPlayerState<AGASPlayerState>();
	check(GASPlayerState);

	AbilitySystemComponent = GASPlayerState->GetAbilitySystemComponent();
	AttributeSet = GASPlayerState->GetAttributeSet();

	AbilitySystemComponent->InitAbilityActorInfo(GASPlayerState, this);
	Cast<UGASAbilitySystemComponent>(AbilitySystemComponent)->AbilityActorInfoSet();

	InitializeDefaultAttributes();
}
