// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/GASPlayerController.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "GASAbilitySystemComponent.h"

AGASPlayerController::AGASPlayerController()
{
	bReplicates = true;
}

void AGASPlayerController::AbilityInputTagPressed(FGameplayTag InputTag)
{
	if (!GetGASASC())
		return;
	GetGASASC()->AbilityInputTagPressed(InputTag);
}

void AGASPlayerController::AbilityInputTagReleased(FGameplayTag InputTag)
{
	if (!GetGASASC())
		return;
	GetGASASC()->AbilityInputTagReleased(InputTag);
}

UGASAbilitySystemComponent* AGASPlayerController::GetGASASC()
{
	if (GASAbilitySystemComponent == nullptr)
	{
		GASAbilitySystemComponent = Cast<UGASAbilitySystemComponent>(
			UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(GetPawn<APawn>()));
	}
	return GASAbilitySystemComponent;
}
