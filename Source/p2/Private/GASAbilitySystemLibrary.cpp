// Fill out your copyright notice in the Description page of Project Settings.


#include "GASAbilitySystemLibrary.h"
#include "GASAbilityTypes.h"

FVector UGASAbilitySystemLibrary::GetDeathImpulse(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if (const FGASGameplayEffectContext* AuraGameplayEffectContext = static_cast<const FGASGameplayEffectContext*>(
		EffectContextHandle.Get()))
	{
		return AuraGameplayEffectContext->GetDeathImpulse();
	}
	return FVector::ZeroVector;
}

void UGASAbilitySystemLibrary::SetDeathImpulse(FGameplayEffectContextHandle& EffectContextHandle,
                                               const FVector& InDeathImpulse)
{
	UE_LOG(LogTemp, Display, TEXT("%s"), *InDeathImpulse.ToString())

	if (FGASGameplayEffectContext* GASGameplayEffectContext = static_cast<FGASGameplayEffectContext*>(
		EffectContextHandle.Get()))
	{
		GASGameplayEffectContext->SetDeathImpulse(InDeathImpulse);
	}
}

FGameplayEffectContextHandle UGASAbilitySystemLibrary::ApplyDamageEffect(const FDamageEffectParams& DamageEffectParams)
{
	const AActor* SourceAvatarActor = DamageEffectParams.SourceAbilitySystemComponent->GetAvatarActor();

	FGameplayEffectContextHandle EffectContextHandle = DamageEffectParams.SourceAbilitySystemComponent->
	                                                                      MakeEffectContext();
	EffectContextHandle.AddSourceObject(SourceAvatarActor);
	SetDeathImpulse(EffectContextHandle, DamageEffectParams.DeathImpulse);
	const FGameplayEffectSpecHandle SpecHandle = DamageEffectParams.SourceAbilitySystemComponent->MakeOutgoingSpec(
		DamageEffectParams.DamageGameplayEffectClass, DamageEffectParams.AbilityLevel, EffectContextHandle);


	DamageEffectParams.TargetAbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data);
	return EffectContextHandle;
}
