// Fill out your copyright notice in the Description page of Project Settings.


#include "ExecCale/MMC_Damage.h"

#include "GASAttributeSet.h"
#include "GASGameplayTags.h"
#include "Kismet/KismetSystemLibrary.h"

UMMC_Damage::UMMC_Damage()
{
	TargetDefense.AttributeToCapture = UGASAttributeSet::GetDefenseAttribute();
	TargetDefense.AttributeSource = EGameplayEffectAttributeCaptureSource::Target;
	TargetDefense.bSnapshot = false;

	RelevantAttributesToCapture.Add(TargetDefense);

	SourceAttack.AttributeToCapture = UGASAttributeSet::GetAttackAttribute();
	SourceAttack.AttributeSource = EGameplayEffectAttributeCaptureSource::Source;
	SourceAttack.bSnapshot = false;

	RelevantAttributesToCapture.Add(SourceAttack);
}

float UMMC_Damage::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	const FGASGameplayTags Tags = FGASGameplayTags::Get();
	float DamageTagValue = Spec.GetSetByCallerMagnitude(Tags.Damage_BaseDamage, false);
	DamageTagValue = FMath::Max(DamageTagValue, .0f);
	
	float Defense = .0f;
	float Attack = .0f;
	GetCapturedAttributeMagnitude(TargetDefense, Spec, EvaluationParameters, Defense);
	GetCapturedAttributeMagnitude(SourceAttack, Spec, EvaluationParameters, Attack);
	Defense = FMath::Max(Defense, .0f);
	Attack = FMath::Max(Attack, .0f);
	
	return Attack - Defense;
}
