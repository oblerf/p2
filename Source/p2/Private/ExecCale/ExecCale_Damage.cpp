// Fill out your copyright notice in the Description page of Project Settings.


#include "ExecCale/ExecCale_Damage.h"

#include "GASAttributeSet.h"
#include "GASGameplayTags.h"

struct GASDamageStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(Attack);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Defense);

	GASDamageStatics()
	{
		DEFINE_ATTRIBUTE_CAPTUREDEF(UGASAttributeSet, Defense, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UGASAttributeSet, Attack, Source, false);
	}
};

static const GASDamageStatics& DamageStatics()
{
	static GASDamageStatics DStatics;
	return DStatics;
}


UExecCale_Damage::UExecCale_Damage()
{
	RelevantAttributesToCapture.Add(DamageStatics().AttackDef);
	RelevantAttributesToCapture.Add(DamageStatics().DefenseDef);
}

void UExecCale_Damage::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams,
                                              FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	TMap<FGameplayTag, FGameplayEffectAttributeCaptureDefinition> TagsToCaptureDefs;
	const FGASGameplayTags Tags = FGASGameplayTags::Get();
	TagsToCaptureDefs.Add(Tags.Attributes_Secondary_Attack, DamageStatics().AttackDef);
	TagsToCaptureDefs.Add(Tags.Attributes_Secondary_Defense, DamageStatics().DefenseDef);

	const UAbilitySystemComponent* SourceASC = ExecutionParams.GetSourceAbilitySystemComponent();
	const UAbilitySystemComponent* TargetASC = ExecutionParams.GetTargetAbilitySystemComponent();

	AActor* SourceAvatar = SourceASC ? SourceASC->GetAvatarActor() : nullptr;
	AActor* TargetAvatar = TargetASC ? TargetASC->GetAvatarActor() : nullptr;

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
	FGameplayEffectContextHandle EffectContextHandle = Spec.GetContext();

	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();
	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	float Damage = 0.f;
	float DamageTagValue = Spec.GetSetByCallerMagnitude(Tags.Damage_BaseDamage, false);

	Damage += DamageTagValue;

	float TargetDefense = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().DefenseDef, EvaluationParameters,
	                                                           TargetDefense);

	float SourceAttack = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().AttackDef, EvaluationParameters,
	                                                           SourceAttack);

	Damage += FMath::Max(0, SourceAttack - TargetDefense);
	const FGameplayModifierEvaluatedData EvaluatedData(UGASAttributeSet::GetIncomingDamageAttribute(),
												   EGameplayModOp::Additive, Damage);
	OutExecutionOutput.AddOutputModifier(EvaluatedData);

}
