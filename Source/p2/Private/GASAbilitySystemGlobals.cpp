// Fill out your copyright notice in the Description page of Project Settings.


#include "GASAbilitySystemGlobals.h"

#include "GASAbilityTypes.h"

FGameplayEffectContext* UGASAbilitySystemGlobals::AllocGameplayEffectContext() const
{
	return new FGASGameplayEffectContext();
}
