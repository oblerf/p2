// Fill out your copyright notice in the Description page of Project Settings.


#include "GASAssetManager.h"

#include "AbilitySystemGlobals.h"
#include "GASGameplayTags.h"

UGASAssetManager& UGASAssetManager::Get()
{
	check(GEngine)

	UGASAssetManager* AssetManager = Cast<UGASAssetManager>(GEngine->AssetManager);
	return *AssetManager;
}

void UGASAssetManager::StartInitialLoading()
{
	Super::StartInitialLoading();
	FGASGameplayTags::InitializeNativeGameplayTags();

	UAbilitySystemGlobals::Get().InitGlobalData();
}
