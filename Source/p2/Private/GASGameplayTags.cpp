#include "GASGameplayTags.h"

#include "GameplayTagsManager.h"

FGASGameplayTags FGASGameplayTags::GameplayTags;

void FGASGameplayTags::InitializeNativeGameplayTags()
{
	GameplayTags.Attributes_Secondary_Attack = UGameplayTagsManager::Get().AddNativeGameplayTag(
		FName("Attributes.Secondary.Attack"), FString());
	GameplayTags.Attributes_Secondary_Defense = UGameplayTagsManager::Get().AddNativeGameplayTag(
		FName("Attributes.Secondary.Defense"), FString());
	GameplayTags.Attributes_Secondary_MaxHealth = UGameplayTagsManager::Get().AddNativeGameplayTag(
		FName("Attributes.Secondary.MaxHealth"), FString());

	GameplayTags.Ability_Attack = UGameplayTagsManager::Get().AddNativeGameplayTag(
		FName("Ability.Attack"), FString());

	GameplayTags.Damage_BaseDamage = UGameplayTagsManager::Get().AddNativeGameplayTag(
		FName("Damage.BaseDamage"), FString());

	GameplayTags.Equip_Weapon_1 = UGameplayTagsManager::Get().AddNativeGameplayTag(
		FName("Equip.Weapon.1"), FString());
}
