# GAS演示Demo说明文档



## 造成伤害的逻辑

在该演示Demo中，玩家ASC在生成时便给予用于攻击的Ability，游玩过程中，玩家可单击鼠标左键TryActivateAbility该Ability

此时，若触发者此时使用的是客户端，在TryActivateAbility时，客户端不仅会向服务器发送触发请求，同时也在本地执行触发后的逻辑：

*[预测]*
在源触发Pawn的坐标向前向向量作射线检测，若检测到带有ASC的Actor，则**生成自定义的Context**，将所需的参数与伤害Effect一起打包为SpecHandle，发送至目标Actor的ASC令其生效。

*[预测]*
因伤害Effect使用*ModMagnitudeCalculation*(MMC)进行相关计算，预测键使得客户端可以预测Modifier相关数据(*GameplayPrediction.h对此也进行了阐述，但尚未找到验证方法，此结论待定*)。
于此同时，可以继承并重载该类的CalculateBaseMagnitude函数来进行伤害的计算。

在AS中设置了一个*IncomingDamage*的Attribute，用于处理伤害值，计算得到的伤害将会更新目标AS上的该属性。

于此同时，针对AS中
*[PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)]*
函数的重载将获取IncomingDamage的变动，并将其体现在HP的削减上。

HP的削减也将同步给客户端。

### 通信机制的设计思路

预测键使得客户端无需等待服务端回传而可直接运行
*[预测]*
部分的逻辑，本次演示demo中这部分逻辑指的是客户端看到的射线检测线条；在其他正规的GAS框架的游戏中，这部分往往涉及到人物骨骼动画或特效，及时地展现相关逻辑有助于减少延迟丢包环境下的游戏体验。

### 生成自定义的Context

该项目定义了一个继承自FGameplayEffectContext的FGASGameplayEffectContext类，该类存储了DeathImpulse属性，这是个FVector属性，代表当角色死亡时为角色施加的推力。
自定义Context主要用于解决Tag仅承载浮点数可能无法满足要求的问题。

***
## 配表与后续拓展相关问题

### 公式配表
配表主要分为两类，一为计算公式配表，例如伤害计算公式，针对此需求进行的配表实现可能有些繁杂，因而只讨论粗浅的想法：

以伤害公式为例，在Demo中，造成伤害是以增加玩家的IncomingDamage属性实现的：在Effect Modifier计算具体伤害值，在AS上进行血量的扣减。
而在实际生产中，一个技能应该如何造成伤害可能是多样的，而伤害如何通过防御进行削减则一般很少进行改变。

我们可以将前者暂定义为“技能伤害公式”或“一次计算”，而将后者定义为“基础伤害公式”或“二次计算”。在“一次计算”中，我们计算玩家自身的属性，得到该次技能可造成的伤害;在“二次计算”中，针对到来的伤害，计算目标的护甲等相关属性得到最终的血量扣除。

“一次计算”可变性最大而“二次计算”基本不变。

私以为可以通过excel表来生成MMC.h类，其主要对一定义好的基类进行继承，如基类可以定义并初始化Source和Target相应的FGameplayEffectAttributeCaptureDefinition成员，
而继承类仅需对CalculateBaseMagnitude(const FGameplayEffectSpec& Spec)这一函数进行重载，并return excel里填写的文本即可

如：

公式名丨具体公式

Damage丨SourceAttack-TargetDefense

则生成MMC_Damage.h文件，CalculateBaseMagnitude函数返回(SourceAttack-TargetDefense)的值

### 装备和道具配表

与公式配表不同，装备和道具本质为Effect，其可以动态生成因而更具有灵活性，且涉及的计算往往较为简单。

且配表时对应属性的列头最好以Tag的形式书写，便于书写逻辑。

如：

装备Id丨装备名丨穿戴部位Tag丨Attributes.Secondary.Attack丨频道设置丨计算方式丨具体数值

0001丨已折断的直剑丨Equip.Weapon.1丨true丨Equip.Weapon丨Add丨10

道具Id丨道具名丨持续策略丨持续时间丨每生效一次所需丨Attributes.Secondary.Health丨频道设置丨计算方式丨具体数值丨Stack方式丨Stack数。。。

0001丨滴石丨Has Duration丨10丨0.1丨true丨Item丨Add丨10丨Target丨3

其中，*穿戴部位Tag*指装备Effect可以通过挂载RemoveOtherGameplayEffectComponent实现装备的切换，在道具的列表中，则可以通过频道（Channel）设置和Stack来控制可生效的道具种类或数量。